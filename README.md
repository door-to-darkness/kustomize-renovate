# Kustomize - Renovate
This repository deploys [Renovate](https://docs.renovatebot.com/) to a Kubernetes cluster. It is currently intended to be deployed by ArgoCD using Kustomize but can be deployed by hand if necessary.
